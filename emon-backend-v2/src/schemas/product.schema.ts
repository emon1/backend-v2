
import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
  code: String,
  location: String
});